# Poverty world map

This notebook serves us to inform ourselves and practice some data analysis and visualisation techniques. We relied heavily on the [work by Laura Viera](https://www.kaggle.com/lauraviera/using-ml-to-allocate-funding-for-development-aid), and made some improvements. Slight improvement in results (just 1%).

## World map of clusters 0, 1 and 2
We applied KMeans clustering to [Unsupervised Learning on Country Data](https://www.kaggle.com/lauraviera/using-ml-to-allocate-funding-for-development-aid/data), a dataset for Kmeans Clustering on Kaggle, to categorise the countries using socio-economic and health factors that determine the overall development of the country.

![World Map 1](world_map1.png)

- Cluster 0 is doing averages all around.
- Cluster 1 has the highest child mortality rate, the least exports and imports, the lowest GDP, extremely low incomes, highest inflation, lowest life expectancy, and the highest age_fertility  rate of 5 children per woman (number of children that would be born to each woman if the current age-fertility rates remain the same). Countries in Cluster 1 are located across Africa and Asia.
- Cluster 2 is doing extremely well with the lowest child mortality, the highest exports and imports, highest gdpp (by a lot), a higher health, significantly higher incomes, the lowest inflation, the highest life expectancy and the lowest age-fertility rate of 1 child per woman (number of children that would be born to each woman if the current age-fertility rates remain the same)
- Note: Health in cluster 0 is lower than health in cluster 1. Very odd.

## Income comparisons cluster 0, 1, and 2

For comparison, to show the size of the divide between clusters:

![Average income per person in countries in cluster 0](plot0_income.png)

![Average income per person in countries in cluster 1](plot1_income.png)

![Average income per person in countries in cluster 2](plot2_income.png)

## World map subclusters of clusters 0 and 1
We also used the [Multidimensional Poverty Measures](https://www.kaggle.com/ophi/mpi) dataset for providing the Dependent Variable for regressions.
The merged dataset with only cluster 0 and 1 combined (cluster 2 excluded) was used for clustering the observations into 3 new subclusters.

![World Map 2](world_map2.png)

## Urban MPI comparisons subclusters of clusters 0 and 1
For comparison, to show the size of the divide by MPI Urban in clusters 0 and 1, the weighted average number of deprivations poor people experience at the same time in Urban areas in 3 subclusters:

![Multidimensional Poverty Index for urban areas within a country in subcluster 0. MPI is the weighted average number of deprivations poor people experience at the same time.](plot_sub0_mpi_urban.png)

![Multidimensional Poverty Index for urban areas within a country in subcluster 1. MPI is the weighted average number of deprivations poor people experience at the same time.](plot_sub1_mpi_urban.png)

![Multidimensional Poverty Index for urban areas within a country in subcluster 2. MPI is the weighted average number of deprivations poor people experience at the same time.](plot_sub2_mpi_urban.png)

